# kakkamato

Simple terminal-based worm game

![huge gif](screenrec/mato.gif "woah")

## Requirements

An UTF-8-friendly terminal and a font that supports emojis. This game was written with double-character-wide emojis in mind (think macOS) so the in-game collectibles might be considered wider than they look.

For hacking, [Dep](https://github.com/golang/dep) is used for dependency management. However, you can just as well go get github.com/nsf/termbox-go yourself.

## Downloads

Pre-built binaries are available for amd64 architectures for macOS and Linux. See the Artifacts section in the Downloads menu above.
