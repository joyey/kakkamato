package main

import (
	"fmt"
	"github.com/nsf/termbox-go"
	"math/rand"
	"os"
	"time"
)

const (
	Width, Height = 80, 24
	GameSpeed     = 75
)

// Lookup tables for drawing
var cheatSheet = [][]rune{
	{'║', '║', '╗', '╔'},
	{'║', '║', '╝', '╚'},
	{'╚', '╔', '═', '═'},
	{'╝', '╗', '═', '═'},
}

var (
	input chan termbox.Event
)

func init() {
	rand.Seed(time.Now().UnixNano())
	input = initInput() // input event queue
}

func main() {
	err := termbox.Init()
	if err != nil {
		panic(err)
	}
	defer termbox.Close()
	termbox.SetInputMode(termbox.InputEsc)

	w, h := termbox.Size()
	if w < Width || h < Height {
		termbox.Close()
		fmt.Printf("Terminal size %d*%d detected, please use at least %d*%d window\n", w, h, Width, Height)
		os.Exit(1)
	}

	titleScreen()

	// initialize game
	currentLevel := 0
	s := *NewGameState(Levels[currentLevel], 0)
	speed := GameSpeed * time.Millisecond
	ticker := time.NewTicker(speed)

	for !s.Dead {
		select {
		case e := <-input:
			if e.Type == termbox.EventKey {
				switch e.Key {
				case termbox.KeyEsc:
					s.Dead = true
				case termbox.KeyArrowUp:
					s.Snake.Direction = Up
				case termbox.KeyArrowDown:
					s.Snake.Direction = Down
				case termbox.KeyArrowLeft:
					s.Snake.Direction = Left
				case termbox.KeyArrowRight:
					s.Snake.Direction = Right
				}
			}
		case <-ticker.C:
			s.Tick()
			if s.CollectedTurds == 10 {
				currentLevel++
				if currentLevel < len(Levels) {
					speed := time.Duration(GameSpeed-currentLevel*10) * time.Millisecond
					restartTicker(&ticker, speed)
					s = *NewGameState(Levels[currentLevel], s.Score)
				} else {
					// Winning \o/
					s.Dead = true
				}
			}
			render(s.Level, s.Snake, s.Turd, s.Score)
			termbox.Flush()
		}
	}
	gameOver()
}

func restartTicker(t **time.Ticker, speed time.Duration) {
	(*t).Stop()
	newTicker := time.NewTicker(speed)
	*t = newTicker
}

func gameOver() {
	putCenteredString("G A M E    O V E R", Height/2, termbox.ColorCyan, termbox.ColorCyan, termbox.ColorMagenta, termbox.ColorMagenta)
	termbox.Flush()
	time.Sleep(1 * time.Second)
	termbox.PollEvent()
}

func render(l Level, snake Snake, turd Turd, score int) {
	for i, r := range l.Data {
		termbox.SetCell(i%Width, i/Width, r, termbox.ColorWhite, termbox.ColorBlack)
	}
	termbox.SetCell(turd.Location.X, turd.Location.Y, '💩', termbox.ColorWhite, termbox.ColorBlack)
	drawSnake(snake)
	s := fmt.Sprintf("%d", score)
	putCenteredString(s, Height-1, termbox.ColorYellow)
}

func putCenteredString(s string, y int, c ...termbox.Attribute) {
	var x int
	for i, r := range s {
		x = (Width-len(s))/2 + i
		termbox.SetCell(x, y, r, c[i%len(c)], termbox.ColorBlack)
	}

}

func drawSnake(s Snake) {
	for i := 0; i < len(s.Pieces)-2; i++ {
		currDir := calculateDir(s.Pieces[i], s.Pieces[i+1])
		nextDir := calculateDir(s.Pieces[i+1], s.Pieces[i+2])
		termbox.SetCell(s.Pieces[i+1].X, s.Pieces[i+1].Y, cheatSheet[currDir][nextDir], termbox.ColorWhite, termbox.ColorBlack)
	}
	tail := s.Pieces[0]
	termbox.SetCell(tail.X, tail.Y, 'o', termbox.ColorMagenta, termbox.ColorBlack)
	Head := s.Pieces[len(s.Pieces)-1]
	termbox.SetCell(Head.X, Head.Y, 'Ö', termbox.ColorCyan, termbox.ColorBlack)
}

func calculateDir(v1, v2 Vector) Direction {
	delta := Vector{v2.X - v1.X, v2.Y - v1.Y}
	switch delta {
	case Vector{-1, 0}:
		return Left
	case Vector{1, 0}:
		return Right
	case Vector{0, 1}:
		return Down
	case Vector{0, -1}:
		return Up
	default:
		// Just provide something for the thing that should not be
		return 666
	}
}

func initInput() chan termbox.Event {
	queue := make(chan termbox.Event)
	go func() {
		for {
			queue <- termbox.PollEvent()
		}
	}()
	return queue
}

func titleScreen() {
	ticker := time.NewTicker(40 * time.Millisecond)
	l := NewLevel(Title)
	s := Snake{GrowCounter: 100, Direction: Down, Pieces: []Vector{l.StartPos}}
	t := Turd{Location: Vector{39, 11}}
	directions := []Direction{Down, Right, Up, Left}
	moves := []int{16, 56, 16, 56}
	var i, c int
	c = moves[i]
	for keyPressed := false; !keyPressed; {
		select {
		case e := <-input:
			if e.Type == termbox.EventKey {
				keyPressed = true
			}
		case <-ticker.C:
			render(l, s, t, 0)
			putCenteredString("Press any key", 23, termbox.ColorYellow)
			termbox.Flush()
			c--
			if c == 0 {
				i = (i + 1) % 4
				c = moves[i]
				s.Direction = directions[i]
			}
			s.Move()
		}
	}
	ticker.Stop()
}
