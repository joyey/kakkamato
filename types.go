package main

import "math/rand"

type Direction int

const (
	Up Direction = iota
	Down
	Left
	Right
)

type Vector struct {
	X, Y int
}

type Turd struct {
	Location Vector
	Value    int
}

func NewTurd(l Level, s Snake) Turd {
	turd := Turd{Value: 1000}
	for {
		x := rand.Intn(Width - 1)
		y := rand.Intn(Height)
		turd.Location = Vector{x, y}
		turdParts := []Vector{turd.Location, Vector{x + 1, y}}
		if l.FreeCells(turdParts...) && !s.at(turdParts...) {
			break
		}
	}
	return turd
}

func (t *Turd) Tick() {
	if t.Value > 100 {
		t.Value -= 10
	}
}

type Snake struct {
	Pieces      []Vector
	Direction   Direction
	GrowCounter int
}

func (s *Snake) Move() {
	oldHead := s.Pieces[len(s.Pieces)-1]
	var newHead Vector
	switch s.Direction {
	case Up:
		newHead = Vector{oldHead.X, oldHead.Y - 1}
	case Down:
		newHead = Vector{oldHead.X, oldHead.Y + 1}
	case Left:
		newHead = Vector{oldHead.X - 1, oldHead.Y}
	case Right:
		newHead = Vector{oldHead.X + 1, oldHead.Y}
	}
	s.Pieces = append(s.Pieces, newHead)
	if s.GrowCounter == 0 {
		s.Pieces = s.Pieces[1:len(s.Pieces)]
	} else {
		s.GrowCounter--
	}
}

func (s Snake) Head() Vector {
	return s.Pieces[len(s.Pieces)-1]
}

func (s Snake) HeadAt(locations ...Vector) bool {
	head := s.Head()
	for _, p := range locations {
		if p == head {
			return true
		}
	}
	return false
}

func (s Snake) at(locations ...Vector) bool {
	for _, p := range s.Pieces {
		for _, l := range locations {
			if p == l {
				return true
			}
		}
	}
	return false
}

func (s Snake) EatsItself() bool {
	return s.HeadAt(s.Pieces[0 : len(s.Pieces)-1]...)
}

type Level struct {
	Data     []rune
	StartPos Vector
}

func NewLevel(data []rune) Level {
	var startPos Vector
	for i, r := range data {
		if r == 'Ö' {
			startPos.X = i % 80
			startPos.Y = i / 80
			data[i] = ' '
			break
		}
	}
	return Level{Data: data, StartPos: startPos}
}

func (l Level) charAt(v Vector) rune {
	return l.Data[v.Y*80+v.X]
}

func (l Level) FreeCells(cells ...Vector) bool {
	for _, c := range cells {
		if l.charAt(c) != ' ' {
			return false
		}
	}
	return true
}

type GameState struct {
	Snake          Snake
	Turd           Turd
	Level          Level
	Score          int
	CollectedTurds int
	Dead           bool
}

// Create an initial GameState for given Level with given initial score
func NewGameState(l Level, score int) *GameState {
	snake := Snake{GrowCounter: 10, Direction: Right, Pieces: []Vector{l.StartPos}}
	s := GameState{
		Level: l,
		Turd:  NewTurd(l, snake),
		Score: score,
		Snake: snake,
	}
	return &s
}

func (s *GameState) Tick() {
	s.Snake.Move()
	s.Turd.Tick()
	s.checkCollisions()
}

func (s *GameState) checkCollisions() {
	if s.Snake.EatsItself() || !s.Level.FreeCells(s.Snake.Head()) {
		s.Dead = true
	} else if s.Snake.HeadAt(s.Turd.Location, Vector{X: s.Turd.Location.X + 1, Y: s.Turd.Location.Y}) {
		s.CollectedTurds++
		s.Score += s.Turd.Value
		s.Turd = NewTurd(s.Level, s.Snake)
		s.Snake.GrowCounter += 10
	}
}
